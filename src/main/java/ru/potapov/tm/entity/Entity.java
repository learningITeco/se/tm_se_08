package ru.potapov.tm.entity;

public interface Entity {
    String getId();
    String getName();
    String getUserId();
}
