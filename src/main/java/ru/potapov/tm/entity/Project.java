package ru.potapov.tm.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Project implements Cloneable, Entity {
    @Nullable private String      id;
    @Nullable private String      name;
    @Nullable private String      description;
    @Nullable private String      userId;
    @Nullable private LocalDateTime   dateStart;
    @Nullable private LocalDateTime   dateFinish;

    public Project(@NotNull String name) {
        this();
        this.name       = name;
    }

    @Override
    public String toString() {
        return "Project [" + getName() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        Project objProject = (Project)obj;
        if ( (this.getName().equals(objProject.getName()))
                && (this.getDescription().equals(objProject.getDescription()))
                && (this.getDateStart().equals(objProject.getDateStart()))
                && (this.getDateFinish().equals(objProject.getDateFinish())) )
            return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + getDescription().hashCode()+getDateStart().hashCode()+getDateFinish().hashCode();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
