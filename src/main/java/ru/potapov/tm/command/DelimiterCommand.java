package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class DelimiterCommand extends AbstractCommand {
    static int i        = 1;
    private int counter = 0;

    public DelimiterCommand(final Bootstrap bootstrap) {
        super(bootstrap);
        counter = i++;
    }

    @NotNull
    @Override
    public String getName() {
        return "--------group " + counter;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "-------------";
    }

    @Override
    public void execute() {

    }
}
