package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {
    public ExitCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Halts the app";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        System.out.println("Buy-buy....");
    }
}
