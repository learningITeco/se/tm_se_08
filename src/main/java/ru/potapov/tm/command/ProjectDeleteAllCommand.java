package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDeleteAllCommand extends AbstractCommand {
    public ProjectDeleteAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "projects-delete-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all project";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getTaskService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getProjectService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
