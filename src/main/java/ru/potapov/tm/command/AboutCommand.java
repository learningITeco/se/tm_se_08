package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;

import java.util.Objects;

@NoArgsConstructor
public class AboutCommand extends AbstractCommand {

    public AboutCommand(@Nullable final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Prints the info about app";
    }

    @Override
    public void execute() {
        String version = getClass().getPackage().getImplementationVersion();

        String s = "App v" + version;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getTerminalService().printlnArbitraryMassage(s);
    }
}
