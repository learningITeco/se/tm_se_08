package ru.potapov.tm.bootstrap;

import ru.potapov.tm.service.*;

public interface ServiceLocator {
    IProjectService getProjectService();
    ITaskService getTaskService();
    ITerminalService getTerminalService();
    IUserService getUserService();
}
