package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IRepository<T> {
    @NotNull Collection<T> findAll();
    @NotNull Collection<T> findAll(String userId);
    @Nullable T findOne(String name);
    @Nullable T findOne(String userId, String name);
    @Nullable T findOneById(String id);
    @Nullable T findOneById(String userId, String id);
    void persist(T t);
    void persist(String userId, T t);
    @NotNull T merge(T tNew);
    @NotNull T merge(String userId, T tNew);
    void remove(T t);
    void remove(String userId, T t);
     void removeAll();
    void removeAll(String userId);
    void removeAll(Collection<T> list);
    void removeAll(String userId, Collection<T> list);
    @NotNull Collection<T> getCollection();
    @NotNull Collection<T> getCollection(String userId);
}
