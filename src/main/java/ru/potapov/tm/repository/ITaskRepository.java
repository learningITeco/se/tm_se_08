package ru.potapov.tm.repository;


import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface ITaskRepository<T> extends IRepository<T> {
    @NotNull Collection findAll(String userId, String idProject);
}
