package ru.potapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.command.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

public interface ITerminalService {
    void initCommands(final Class[] CLASSES);
    void regestry(AbstractCommand command);
    @NotNull Collection<AbstractCommand> getListCommands();
    @NotNull Map<String, AbstractCommand> getMapCommands();
    @Nullable LocalDateTime inputDate(String massage);
    @NotNull Scanner getIn() ;
    @NotNull DateTimeFormatter getFt() ;
    @NotNull String readLine(String msg);
    void printMassageNotAuthorized();
    void printMassageCompleted();
    void printMassageOk();
    void printlnArbitraryMassage(String msg);
    void printArbitraryMassage(String msg);
}
