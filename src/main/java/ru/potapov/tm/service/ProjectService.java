package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.repository.IProjectRepository;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectService implements IProjectService {
    @Nullable private IProjectRepository<Project> projectRepository;

    public ProjectService(@NotNull final IProjectRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public int checkSize(){
        if (Objects.isNull(projectRepository))
            return 0;
        return projectRepository.getCollection().size();
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name){
        if (Objects.isNull(projectRepository))
            return null;
        return projectRepository.findOne(name);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String userId, @NotNull final String name){
        if (Objects.isNull(projectRepository))
            return null;
        return projectRepository.findOne(userId, name);
    }

    @NotNull
    @Override
    public Collection<Project> getCollection(@NotNull final String userId){
        if (Objects.isNull(projectRepository))
            return new ArrayList<>();

        return projectRepository.getCollection(userId);
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException{
        if (Objects.nonNull(name) && Objects.nonNull(projectRepository)){
            Project newProject = (Project) project.clone();
            newProject.setName(name);
            return projectRepository.merge(newProject);
        }
        return project;
    }

    @Override
    public void removeAll(@NotNull final String userId){
        if (Objects.isNull(projectRepository))
            return;
        projectRepository.removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Project> listProjects){
        if (Objects.isNull(projectRepository))
            return;
        projectRepository.removeAll(listProjects);
    }

    @Override
    public void remove(@NotNull final Project project){
        if (Objects.isNull(projectRepository))
            return;
        projectRepository.remove(project);
    }

    @Override
    public void put(@NotNull final Project project){
        if (Objects.isNull(projectRepository))
            return;
        projectRepository.merge(project);
    }

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener, @NotNull final DateTimeFormatter ft){
        String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + project.getDateStart().format(ft) + "\n";
        res += "Date finish: " + project.getDateFinish().format(ft) + "\n";

        return res;
    }
}
