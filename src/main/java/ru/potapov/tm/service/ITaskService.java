package ru.potapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.ITaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface ITaskService {
    @NotNull ITaskRepository<Task> getTaskRepository();
    int checkSize();
    @Nullable Task findTaskByName(String name);
    void remove(Task task);
    void removeAll(String userId);
    void removeAll(Collection<Task> listTasks);
    @NotNull Task renameTask(Task task, String name) throws CloneNotSupportedException;
    void changeProject(Task task, Project project) throws CloneNotSupportedException;
    @NotNull Collection<Task> findAll(String idProject);
    @NotNull Collection<Task> findAll(String userId, String idProject);
    void put(Task task);
    @NotNull String collectTaskInfo(Project project, Task task, DateTimeFormatter ft);
}
