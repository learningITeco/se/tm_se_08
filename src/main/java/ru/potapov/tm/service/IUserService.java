package ru.potapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.UUID;

public interface IUserService {
    @NotNull User create(String name, String hashPass, RoleType role);
    @Nullable User getUserByName(String name);
    @Nullable User getUserById(String id);
    boolean isUserPassCorrect(User user, String hashPass);
    @NotNull Collection<User> getCollectionUser();
    @NotNull User changePass(User user, String newHashPass) throws CloneNotSupportedException;
    void put(User user);
    @NotNull String collectUserInfo(User user);
    void createPredefinedUsers();
    @Nullable MessageDigest getMd();
    boolean isAuthorized();
    void setAuthorized(boolean authorized);
    @Nullable User getAuthorizedUser();
    void setAuthorizedUser(User authorizedUser);
}
